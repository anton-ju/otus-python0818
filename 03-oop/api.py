#!/usr/bin/env python
# -*- coding: utf-8 -*-

import json
import datetime
import logging
import hashlib
import uuid
from optparse import OptionParser
from http.server import HTTPServer, BaseHTTPRequestHandler
import scoring
from weakref import WeakKeyDictionary

SALT = "Otus"
ADMIN_LOGIN = "admin"
ADMIN_SALT = "42"
OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
ERRORS = {
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
}
UNKNOWN = 0
MALE = 1
FEMALE = 2
GENDERS = {
    UNKNOWN: "unknown",
    MALE: "male",
    FEMALE: "female",
}


class Field(object):
    def __init__(self, required=False, nullable=False):
        self.required = required
        self.nullable = nullable
        self.value = WeakKeyDictionary()

    def __get__(self, instance, owner):
        return self.value.get(instance)

    def __set__(self, instance, value):
        self._validate(value)
        self.value[instance] = value

    def _validate(self, value):
        if self.required and value is None:
            raise ValueError(f'{self.__class__.__name__} -> value is None')

        if not self.nullable and value == type(value)():
            raise ValueError(f'{self.__class__.__name__} -> null value')


class CharField(Field):

    def _validate(self, value):
        super()._validate(value)
        if not (value is None) and not isinstance(value, str):
            raise TypeError(f'{self.__class__.__name__} -> expected string')


class ArgumentsField(Field):

    def _validate(self, value):
        super()._validate(value)
        if not isinstance(value, dict):
            raise TypeError(f'{self.__class__.__name__} -> expected dict')


class EmailField(CharField):

    def _validate(self, value):
        super()._validate(value)
        if not (value is None) and value != '':
            if '@' not in value:
                raise ValueError(f'{self.__class__.__name__} -> invalid format')


class PhoneField(Field):

    def _validate(self, value):
        super()._validate(value)
        if not (value is None) and value != '':
            if isinstance(value, int) or isinstance(value, str):
                value = str(value)
                if len(value) != 11 or value[0] != '7':
                    raise ValueError(f'{self.__class__.__name__} -> invalid format')
                try:
                    int(value)
                except ValueError:
                    raise ValueError(f'{self.__class__.__name__} -> invalid format')
            else:
                raise TypeError(f'{self.__class__.__name__} -> expected string or int')


class DateField(Field):

    def _validate(self, value):
        super()._validate(value)
        if isinstance(value, str):
            try:
                datetime.datetime.strptime(value, '%d.%m.%Y')
            except ValueError as err:
                raise ValueError(f'{self.__class__.__name__} -> Incorrect data format, expected DD.MM.YYYY')
        elif not (value is None):
            raise ValueError(f'{self.__class__.__name__} -> expected string')


class BirthDayField(DateField):

    def _validate(self, value):
        super()._validate(value)
        if not (value is None):
            date = datetime.datetime.strptime(value, '%d.%m.%Y')
            if (datetime.date.today().year - date.year > 70) or (datetime.date.today().year - date.year <= 0):
                raise ValueError(f'{self.__class__.__name__} -> invalid date')


class GenderField(Field):

    def _validate(self, value):
        super()._validate(value)
        if value is not None:
            if not isinstance(value, int):
                raise TypeError(f'{self.__class__.__name__} -> not an int')
            elif value not in (0, 1, 2):
                raise ValueError(f'{self.__class__.__name__} -> invalid format')


class ClientIDsField(Field):
    def _validate(self, value):
        super()._validate(value)
        if value is not None:
            if not isinstance(value, list):
                raise TypeError(f'{self.__class__.__name__} -> not a list')
            else:
                not_int = [x for x in value if not isinstance(x, int)]
                if not_int:
                    raise TypeError(f'{self.__class__.__name__} -> expected list of integers')


class Request:
    def __init__(self):
        self.errors = []

    def is_valid(self):
        if self.errors:
            raise ValueError(''.join([f'{e}\n' for e in self.errors]))


class ClientsInterestsRequest(Request):
    client_ids = ClientIDsField(required=True)
    date = DateField(required=False, nullable=True)

    def __init__(self, request):
        super().__init__()
        try:
            self.client_ids = request.get('client_ids')
            self.date = request.get('date')
        except (TypeError, ValueError) as e:
            self.errors.append(str(e))
        self.is_valid()


class OnlineScoreRequest(Request):
    first_name = CharField(required=False, nullable=True)
    last_name = CharField(required=False, nullable=True)
    email = EmailField(required=False, nullable=True)
    phone = PhoneField(required=False, nullable=True)
    birthday = BirthDayField(required=False, nullable=True)
    gender = GenderField(required=False, nullable=True)

    def __init__(self, request):
        super().__init__()
        try:
            self.first_name = request.get('first_name')
            self.last_name = request.get('last_name')
            self.phone = request.get('phone')
            self.birthday = request.get('birthday')
            self.email = request.get('email')
            self.gender = request.get('gender')
        except Exception as e:
            self.errors.append(str(e))
        self.osr_is_valid()
        self.is_valid()

    def osr_is_valid(self):
        if not (bool(self.first_name and self.last_name)
                or bool(self.email and self.phone)
                or bool(self.gender in (0, 1, 2) and self.birthday)):
            self.errors.append('invalid OnlineScoreRequest')


class MethodRequest(Request):
    account = CharField(required=False, nullable=True)
    login = CharField(required=True, nullable=True)
    token = CharField(required=True, nullable=True)
    arguments = ArgumentsField(required=True, nullable=True)
    method = CharField(required=True, nullable=False)

    def __init__(self, request):
        super().__init__()
        self.account = request.get('account')
        self.login = request.get('login')
        self.method = request.get('method')
        self.token = request.get('token')
        self.arguments = request.get('arguments')
        self.is_valid()

    @property
    def is_admin(self):
        return self.login == ADMIN_LOGIN


def check_auth(request):
    if request.is_admin:
        digest = hashlib.sha512((datetime.datetime.now().strftime("%Y%m%d%H") + ADMIN_SALT).encode()).hexdigest()
    else:
        digest = hashlib.sha512((request.account + request.login + SALT).encode()).hexdigest()
    if digest == request.token:
        return True
    return False


def online_score_method(m_request, ctx, store):
    try:
        ors = OnlineScoreRequest(m_request.arguments)
    except Exception as e:
        code = INVALID_REQUEST
        response = {'error': f'{ERRORS[code]}: {str(e)}'}
        return response, code

    if m_request.is_admin:
        score = 42
    else:
        score = scoring.get_score(
            store,
            ors.phone,
            ors.email,
            birthday=ors.birthday,
            gender=ors.gender,
            first_name=ors.first_name,
            last_name=ors.last_name
        )
    response = {'score': score}
    code = OK
    ctx['has'] = [k for k, v in m_request.arguments.items() if (v is not None) and (v != type(v))]
    return response, code


def clients_interests_method(m_request, ctx, store):
    try:
        cir = ClientsInterestsRequest(m_request.arguments)
    except Exception as e:
        code = INVALID_REQUEST
        response = {'error': f'{ERRORS[code]}: {str(e)}'}
        return response, code

    interests = {}
    for cid in cir.client_ids:
        interests[cid] = scoring.get_interests(store, cid)

    response = interests
    code = OK
    ctx['nclients'] = len(cir.client_ids)

    return response, code


def method_handler(request, ctx, store):
    response, code = None, None
    body = request.get('body')

    if body is None or body == {}:
        code = INVALID_REQUEST
        response = {'error': ERRORS[code]}
        return response, code

    try:
        m_request = MethodRequest(body)
    except Exception as e:
        code = INVALID_REQUEST
        response = {'error': f'{ERRORS[code]}: {str(e)}'}
        return response, code

    if not check_auth(m_request):
        code = FORBIDDEN
        response = {'error': f'{ERRORS[code]}'}
        return response, code

    if m_request.method == 'online_score':
        response, code = online_score_method(m_request, ctx, store)

    if m_request.method == 'clients_interests':
        response, code = clients_interests_method(m_request, ctx, store)

    return response, code


class MainHTTPHandler(BaseHTTPRequestHandler):
    router = {
        "method": method_handler
    }
    store = None

    def get_request_id(self, headers):
        return headers.get('HTTP_X_REQUEST_ID', uuid.uuid4().hex)

    def do_POST(self):
        response, code = {}, OK
        context = {"request_id": self.get_request_id(self.headers)}
        request = None
        try:
            data_string = self.rfile.read(int(self.headers['Content-Length']))
            request = json.loads(data_string)
        except:
            code = BAD_REQUEST

        if request:
            path = self.path.strip("/")
            logger.info("%s: %s %s" % (self.path, data_string, context["request_id"]))
            if path in self.router:
                try:
                    response, code = self.router[path]({"body": request, "headers": self.headers}, context, self.store)
                except Exception as e:
                    logger.exception("Unexpected error: %s" % e)
                    code = INTERNAL_ERROR
            else:
                code = NOT_FOUND

        self.send_response(code)
        self.send_header("Content-Type", "application/json")
        self.end_headers()
        if code not in ERRORS:
            r = {"response": response, "code": code}
        else:
            r = {"error": response or ERRORS.get(code, "Unknown Error"), "code": code}
        context.update(r)
        logger.info(context)
        self.wfile.write(json.dumps(r))
        return


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logging.basicConfig(filename=opts.log, level=logging.INFO,
                        format='[%(asctime)s] %(levelname).1s %(message)s', datefmt='%Y.%m.%d %H:%M:%S')
    logger = logging.getLogger(__name__)

    if opts.log:
        fh = logging.FileHandler(opts.log)
        logger.addHandler(fh)
    ch = logging.StreamHandler()
    logger.addHandler(ch)
    server = HTTPServer(("localhost", opts.port), MainHTTPHandler)
    logger.info("Starting server at %s" % opts.port)

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
    server.server_close()
