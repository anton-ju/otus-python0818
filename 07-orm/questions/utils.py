def shift_vote(rating, direction):

    if str.upper(direction) == 'UP':
        result = 1 if rating > 0 else rating + 1
    elif str.upper(direction) == 'DOWN':
        result = -1 if rating < 0 else rating - 1
    else:
        result = rating
    return result
