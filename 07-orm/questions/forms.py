from django import forms
from django.core.exceptions import ValidationError

from .models import Question, Answer


class AnswerCreateForm(forms.ModelForm):

    class Meta:
        model = Answer
        fields = ('content', 'author', 'question',)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.fields['author'].widget = forms.HiddenInput()
        self.fields['question'].widget = forms.HiddenInput()


class QuestionCreateForm(forms.ModelForm):

    tags_text = forms.CharField(max_length=100)

    class Meta:
        model = Question
        fields = ('title', 'content', )

    def clean_tags_text(self):
        data = self.cleaned_data['tags_text']
        tags = data.split(',')
        if len(tags) > 3:
            raise ValidationError('Maximum 3 tags')
        return tags
