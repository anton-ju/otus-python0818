from django.urls import path, re_path

from rest_framework_simplejwt.views import token_obtain_pair, token_refresh

from .views import QuestionAPIView, QuestionDetailAPIView, AnswerAPIView

urlpatterns = [
    path('v1/questions/', QuestionAPIView.as_view(), name='get_questions'),
    path('v1/questions/<int:pk>', QuestionDetailAPIView.as_view(), name='get_question_detail'),
    path('v1/questions/<int:pk>/answers', AnswerAPIView.as_view(), name='get_answers'),
    path('v1/token/', token_obtain_pair, name='token_obtain'),
    path('v1/token/refresh', token_refresh, name='token_refresh'),
]

