from rest_framework import serializers
from ..models import Question, Answer


class QuestionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Question
        fields = ('author', 'title', 'content', 'date', 'tags', 'correct_answer',)


class AnswerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Answer
        fields = ('author', 'content', 'date', 'question')
