from django.db.models import Q
from django.shortcuts import get_object_or_404

from rest_framework.generics import ListAPIView, RetrieveAPIView
from rest_framework.permissions import IsAuthenticated

from .serializers import QuestionSerializer, AnswerSerializer
from ..models import Question


class QuestionAPIView(ListAPIView):
    serializer_class = QuestionSerializer
    model = serializer_class.Meta.model
    queryset = Question.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_queryset(self, *args, **kwargs):
        trending = self.request.GET.get('trending')
        if trending:
            queryset = self.model.with_rating.by_rating()
        else:
            queryset = self.model.with_rating.by_date()
        q = self.request.GET.get('q')
        if q:
            q = q.strip()
            if q.startswith('tag:'):
                tag = q[4:]
                queryset = queryset.filter(tags__keyword__icontains=tag)
            else:
                queryset = queryset.filter(Q(title__icontains=q) | Q(content__icontains=q))

        return queryset


class QuestionDetailAPIView(RetrieveAPIView):
    serializer_class = QuestionSerializer
    model = serializer_class.Meta.model
    queryset = model.objects.all()
    permission_classes = (IsAuthenticated,)


class AnswerAPIView(ListAPIView):
    serializer_class = AnswerSerializer
    model = serializer_class.Meta.model
    permission_classes = (IsAuthenticated,)

    def get_queryset(self, *args, **kwargs):
        question_id = self.kwargs[self.lookup_field]
        question = get_object_or_404(Question, pk=question_id)
        return question.answers.all()
