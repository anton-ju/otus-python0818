from django.db.models import Sum
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.test import TestCase
from django.urls import reverse

from ..models import Question, Answer, QuestionVotes, AnswerVotes


class QuestionModelTest(TestCase):
    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = get_user_model().objects.create_user('admin', password='adminpass')
        cls.user.save()
        cls.question = Question(title='test question 1',
                                content='some content',
                                author=cls.user)
        cls.question.save()

        cls.user1 = get_user_model().objects.create_user('user1', password='user1pass')
        cls.user1.save()
        cls.user2 = get_user_model().objects.create_user('user2', password='user2pass')
        cls.user2.save()

    def test_get_answers_count(self):
        """ Answers count"""
        result = self.question.get_answers_count()
        self.assertEqual(result, 0)
        a = Answer(content='answer on question 1',
                   author=self.user,
                   question=self.question)
        a.save()
        result = self.question.get_answers_count()
        self.assertEqual(result, 1)

    def test_get_absolute_url(self):
        expected = '/question/%s' % self.question.id
        result = self.question.get_absolute_url()
        self.assertEqual(result, expected)

    def test_set_vote_down(self):
        """ User set vote down"""
        self.question.set_vote(self.user, 'down')
        result = self.question.votes.get(author=self.user).vote
        self.assertEqual(result, -1)

    def test_set_vote_down_twice(self):
        """ User set vote down twice"""
        self.question.set_vote(self.user, 'down')
        self.question.set_vote(self.user, 'down')
        result = self.question.votes.get(author=self.user).vote
        self.assertEqual(result, -1)

    def test_set_vote_down_up(self):
        """ User set vote down and after ser vote up"""
        self.question.set_vote(self.user, 'down')
        self.question.set_vote(self.user, 'up')
        result = self.question.votes.get(author=self.user).vote
        self.assertEqual(result, 0)

    def test_set_vote_up(self):
        """ User set vote up"""
        self.question.set_vote(self.user, 'up')
        result = self.question.votes.get(author=self.user).vote
        self.assertEqual(result, 1)

    def test_get_votes_sum(self):
        """ Multuple users set votes"""
        result = self.question.get_votes_sum()
        self.assertEqual(result, 0)

        self.question.set_vote(self.user, 'up')
        self.question.set_vote(self.user1, 'up')
        self.question.set_vote(self.user2, 'down')
        result = self.question.get_votes_sum()
        self.assertEqual(result, 1)

    def test_get_votes_count(self):
        """ votes count """
        result = self.question.get_votes_count()
        self.assertEqual(result, 0)

        self.question.set_vote(self.user, 'up')
        self.question.set_vote(self.user1, 'down')

        result = self.question.get_votes_count()
        self.assertEqual(result, 2)

        # user remove his vote
        self.question.set_vote(self.user, 'down')
        result = self.question.get_votes_count()
        self.assertEqual(result, 1)

    def test_custom_manager(self):
        """ custom manager has rating attribute"""
        result = Question.with_rating.all()
        # custom manager adds rating attribute
        self.assertTrue(hasattr(result[0], 'rating'))
        # correct attribute value rating
        self.question.set_vote(self.user1, 'up')
        self.question.set_vote(self.user2, 'up')
        result = Question.with_rating.all()
        self.assertEqual(result[0].rating, 2)

    def test_custom_manager_sort_by_rating(self):
        """ custom manager sorting by rating"""
        question = Question(title='test question 2',
                            content='some content 2',
                            author=self.user)
        question.save()
        self.question.set_vote(self.user1, 'up')
        result = Question.with_rating.by_rating()
        self.assertEqual(result[0].title, 'test question 1')
        self.assertEqual(result[1].title, 'test question 2')

    def test_custom_manager_sort_same_rating(self):
        """ custom manager sorting same rating"""
        # add another question with later date and 0 rating check its position in query
        question = Question(title='test question 2',
                            content='some content 2',
                            author=self.user)
        question.save()
        # questions with same rating sorted by date
        result = Question.with_rating.by_rating()
        self.assertEqual(result[0].title, 'test question 2')
        self.assertEqual(result[1].title, 'test question 1')

    def test_custom_manager_sort_by_date(self):
        """ custom manager sorting by date"""
        question = Question(title='test question 2',
                            content='some content 2',
                            author=self.user)
        question.save()
        result = Question.with_rating.by_date()
        self.assertEqual(result[0].title, 'test question 2')
        self.assertEqual(result[1].title, 'test question 1')

class AnswerModelTest(TestCase):

    @classmethod
    def setUpClass(cls):
        super().setUpClass()
        cls.user = get_user_model().objects.create_user('admin', password='adminpass')
        cls.user.save()
        cls.question = Question(title='test question 1',
                                content='some content',
                                author=cls.user)
        cls.question.save()

        cls.answer1 = Answer(author=cls.user,
                             content='test answer 1',
                             question=cls.question)
        cls.answer1.save()
        cls.answer1.set_correct()

        cls.answer2 = Answer(author=cls.user,
                             content='test answer 2',
                             question=cls.question)
        cls.answer2.save()
        cls.user1 = get_user_model().objects.create_user('user1', password='user1pass')
        cls.user1.save()
        cls.user2 = get_user_model().objects.create_user('user2', password='user2pass')
        cls.user2.save()

    def test_set_correct(self):
        self.answer2.set_correct()
        self.assertEqual(self.question.correct_answer, self.answer2)

    def test_is_correct(self):
        self.assertEqual(self.question.correct_answer, self.answer1)
        self.assertTrue(self.answer1.is_correct())
