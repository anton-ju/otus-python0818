from django.db.models import Sum
from django.contrib.auth import get_user_model
from django.contrib.auth.models import User
from django.test import TestCase, RequestFactory
from django.urls import reverse

from ..views import Vote, SetCorrect
from ..models import Answer, Question


class AskViewTest(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user('user1', password='user1pass')

    def test_ask_view_loggedin_status_code(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse('questions:ask'))
        self.assertEqual(response.status_code, 200)

    def test_ask_view_loggedout_status_code(self):
        self.client.logout()
        response = self.client.get(reverse('questions:ask'))
        self.assertRedirects(response, '/login/?next=/ask/')


class IndexViewTest(TestCase):
    def test_index_view_status_code(self):
        response = self.client.get(reverse('questions:index'))
        self.assertEqual(response.status_code, 200)


class VoteView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = get_user_model().objects.create_user('user', password='userpass')
        self.question = Question(title='test question 1',
                                 content='some content',
                                 author=self.user)
        self.question.save()

        self.answer = Answer(content='42',
                             author=self.user,
                             question=self.question)
        self.answer.save()

    def test_vote_question_redirect(self):
        kwargs = {}
        kwargs['iid'] = self.question.id
        kwargs['model'] = 'question'
        kwargs['vote'] = 'up'
        self.client.force_login(self.user)
        response = self.client.get(reverse('questions:vote', kwargs=kwargs))
        redirect_url = reverse('questions:question', kwargs={'qid': self.question.id})
        self.assertRedirects(response, redirect_url)

    def test_vote_answer_redirect(self):
        kwargs = {}
        kwargs['iid'] = self.answer.id
        kwargs['model'] = 'answer'
        kwargs['vote'] = 'down'

        self.client.force_login(self.user)
        response = self.client.get(reverse('questions:vote', kwargs=kwargs))
        redirect_url = reverse('questions:question', kwargs={'qid': self.question.id})
        self.assertRedirects(response, redirect_url)


class SetCorrectView(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = get_user_model().objects.create_user('user', password='userpass')
        self.question = Question(title='test question 1',
                                 content='some content',
                                 author=self.user)
        self.question.save()
        self.answer = Answer(content='42',
                             author=self.user,
                             question=self.question)
        self.answer.save()
        self.client.force_login(self.user)

        kwargs = {}
        kwargs['aid'] = self.answer.id
        self.response = self.client.get(reverse('questions:set_correct', kwargs=kwargs), follow=True)

    def test_contains_checked_answer(self):
        """ Answer marked as correct"""
        self.assertContains(self.response, 'fa-check-square')

    def test_redirect_url(self):
        """ SetCorrect redirects to correct url"""
        redirect_url = reverse('questions:question', kwargs={'qid': self.question.id})
        self.assertRedirects(self.response, redirect_url)
