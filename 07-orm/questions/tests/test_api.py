from django.contrib.auth import get_user_model
from django.urls import reverse

from rest_framework.test import APITestCase

from ..api.views import QuestionAPIView, QuestionDetailAPIView, AnswerAPIView
from ..api.serializers import AnswerSerializer, QuestionSerializer
from ..models import Question, Answer


class QuestionAPIViewTest(APITestCase):

    def get_token(self):
        response = self.client.post(reverse('questions:token_obtain'), self.credentials)
        if response.status_code == 200:
            self.token = response.data
            return self.token
        elif response.status_code == 401:
            raise RuntimeError('Invalid Credentials')
        else:
            raise RuntimeError('status code %s %s' % (response.status_code, response.data))

    def get_access_token(self):
        if not hasattr(self, 'token'):
            return self.get_token()['access']
        refresh_token = self.token['refresh']
        response = self.client.post(reverse('questions:token_refresh'), refresh=refresh_token)
        if response.status_code == 200:
            return response.data['access']
        else:
            return self.get_token()['access']

    def setUp(self):
        self.username = 'user'
        self.password = 'userpass'
        self.credentials = {'username': self.username, 'password': self.password}
        self.user = get_user_model().objects.create_user(self.username, password=self.password)
        self.accsess_token = self.get_access_token()
        self.question = Question(title='test question 1',
                                 content='some content',
                                 author=self.user)
        self.question.save()

        self.answer = Answer(content='42',
                             author=self.user,
                             question=self.question)
        self.answer.save()

        self.answer = Answer(content='main answer',
                             author=self.user,
                             question=self.question)
        self.answer.save()

    def test_get_questions(self):
        auth = 'Bearer {0}'.format(self.accsess_token)
        url = reverse('questions:get_questions')
        response = self.client.get(url, HTTP_AUTHORIZATION=auth, format='json')
        self.assertEqual(response.status_code, 200)
        serializer = QuestionSerializer(Question.objects.all(), many=True)
        self.assertEqual(response.data['results'], serializer.data)

    def test_get_answers(self):

        auth = 'Bearer {0}'.format(self.accsess_token)
        url = reverse('questions:get_answers', kwargs={'pk': self.question.id})
        response = self.client.get(url, HTTP_AUTHORIZATION=auth, format='json')
        self.assertEqual(response.status_code, 200)
        serializer = AnswerSerializer(self.question.answers.all(), many=True)
        self.assertEqual(response.data['results'], serializer.data)

    def test_get_question_detail(self):

        auth = 'Bearer {0}'.format(self.accsess_token)
        url = reverse('questions:get_question_detail', kwargs={'pk': self.question.id})
        response = self.client.get(url, HTTP_AUTHORIZATION=auth, format='json')
        self.assertEqual(response.status_code, 200)
        serializer = QuestionSerializer(self.question)
        self.assertEqual(response.data, serializer.data)
