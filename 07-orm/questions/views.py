from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.paginator import Paginator
from django.db import transaction
from django.db.models import Q
from django.http import Http404, HttpResponseRedirect
from django.views.generic import ListView, CreateView, View
from django.views.generic.edit import BaseFormView
from django.views.generic.list import MultipleObjectMixin, MultipleObjectTemplateResponseMixin
from django.views.generic.list import TemplateResponseMixin
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.shortcuts import render, redirect, get_object_or_404

from .forms import QuestionCreateForm, AnswerCreateForm
from .models import Answer, Question


class IndexView(ListView):
    model = Question
    template_name = 'questions/index.html'
    context_object_name = 'question_list'
    queryset = model.with_rating.by_date()
    paginate_by = 20
    extra_context = {'trending_list': Question.with_rating.by_rating}

    def get_queryset(self):
        order = self.request.GET.get('order')
        if order == 'date':
            queryset = self.model.with_rating.by_date()
        elif order == 'rating':
            queryset = self.model.with_rating.by_rating()
        else:
            queryset = super().get_queryset()

        q = self.request.GET.get('q')
        if q:
            q = q.strip()
            if q.startswith('tag:'):
                tag = q[4:]
                queryset = queryset.filter(tags__keyword__icontains=tag)
            else:
                queryset = queryset.filter(Q(title__icontains=q) | Q(content__icontains=q))

        return queryset


class QuestionCreateVeiw(LoginRequiredMixin, CreateView):
    model = Question
    form_class = QuestionCreateForm
    success_url = reverse_lazy('questions:index')
    template_name = 'questions/question_create.html'

    def form_valid(self, form):
        form.instance.author = self.request.user
        obj = form.save()
        obj.add_tags_from_list(form.cleaned_data['tags_text'])
        return super().form_valid(form)


class AnswerListAndCreate(MultipleObjectTemplateResponseMixin,
                          TemplateResponseMixin, MultipleObjectMixin, BaseFormView):
    """ Take positional argument 'qid' with question id
        updates context variable with instance of Question
        and trenging_list
    """
    model = Answer
    form_class = AnswerCreateForm
    paginate_by = 30
    template_name = 'questions/question_detail.html'
    success_url = 'questions:question'

    def get_context_data(self, **kwargs):
        question = get_object_or_404(Question, pk=self.kwargs.get('qid'))
        kwargs['question'] = question
        kwargs['trending_list'] = Question.with_rating.by_rating
        return super().get_context_data(**kwargs)

    def get_queryset(self):
        return Answer.with_rating.filter(question=self.kwargs.get('qid')).all()

    def get(self, request, *args, **kwargs):
        """ method copyed from BaseListView to keep pagination"""

        self.object_list = self.get_queryset()
        allow_empty = self.get_allow_empty()
        if not allow_empty:
            # When pagination is enabled and object_list is a queryset,
            # it's better to do a cheap query than to load the unpaginated
            # queryset in memory.
            if self.get_paginate_by(self.object_list) is not None and hasattr(self.object_list, 'exists'):
                is_empty = not self.object_list.exists()
            else:
                is_empty = not self.object_list
            if is_empty:
                raise Http404(_("Empty list and '%(class_name)s.allow_empty' is False.") % {
                    'class_name': self.__class__.__name__,
                })
        context = self.get_context_data()
        return self.render_to_response(context)

    def get_initial(self):
        question = get_object_or_404(Question, pk=self.kwargs.get('qid'))
        initial = {'author': self.request.user, 'question': question}
        return initial

    def form_valid(self, form):
        form.save()
        return redirect(self.get_success_url(), self.kwargs.get('qid'))


class SetCorrect(LoginRequiredMixin, View):
    def get(self, request, **kwargs):
        aid = kwargs.get('aid')
        user = request.user
        if aid:
            instance = get_object_or_404(Answer, pk=aid)
            redirect_args = instance.question.id

        else:
            return redirect('questions:index')

        if instance.question.author == user:
            instance.set_correct()
            instance.save()
        return redirect('questions:question', redirect_args)


class Vote(LoginRequiredMixin, View):
    def get(self, request, **kwargs):
        instance_id = kwargs.get('iid')
        user = request.user
        action = kwargs.get('vote')
        model = kwargs.get('model')
        if model == 'question':
            instance = get_object_or_404(Question, pk=instance_id)
            redirect_args = instance_id
        elif model == 'answer':
            instance = get_object_or_404(Answer, pk=instance_id)
            redirect_args = instance.question.id

        else:
            return redirect('questions:index')

        instance.set_vote(user, action)
        instance.save()

        return redirect('questions:question', redirect_args)
