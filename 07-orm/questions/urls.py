from django.urls import path, re_path, include

from . import views

app_name = 'questions'
urlpatterns = [
    path('', views.IndexView.as_view(), name='index'),
    path('api/', include('questions.api.urls')),
    path('ask/', views.QuestionCreateVeiw.as_view(), name='ask'),
    path('question/<int:qid>', views.AnswerListAndCreate.as_view(), name='question'),
    path('answer/<int:aid>/correct', views.SetCorrect.as_view(), name='set_correct'),
    path('<model>/<int:iid>/<vote>', views.Vote.as_view(), name='vote'),
]
