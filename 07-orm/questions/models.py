from django.contrib.auth.models import User
from django.conf import settings
from django.core.exceptions import ObjectDoesNotExist
from django.db import models, transaction
from django.db.models import Sum, Manager
from django.db.models.functions import Coalesce
from django.utils import timezone

from .utils import shift_vote


class WithRatingManager(models.Manager):
    def get_queryset(self):
        q = super().get_queryset()
        return q.annotate(rating=Coalesce(Sum('votes__vote'), 0))

    def by_date(self):
        return self.get_queryset().order_by('-date')

    def by_rating(self):
        return self.get_queryset().order_by('-rating', '-date')


class Authorable(models.Model):
    author = models.ForeignKey(User, on_delete=models.CASCADE)

    class Meta:
        abstract = True


class VotesMixin:
    """ Add methods to get and set vote
    """
    def get_votes_count(self):
        return self.votes.filter(vote__in=(-1, 1)).count()

    def get_votes_sum(self):
        result = self.votes.aggregate(Sum('vote'))
        return result['vote__sum'] or 0

    def set_vote(self, user, shift_direction):
        self.votes.model.set_vote(self, user, shift_direction)


class Voteable(models.Model):
    """ Abstract class to add vote attribute and related methods
    """
    vote = models.IntegerField(default=0)

    class Meta:
        abstract = True

    @classmethod
    @transaction.atomic
    def set_vote(cls, entity, user, shift_direction):
        v, _ = cls.objects.get_or_create(
            entity=entity,
            author=user,
            defaults={'vote': shift_vote(0, shift_direction)}
        )
        new_vote = shift_vote(v.vote, shift_direction)
        v.vote = new_vote
        v.save()

    @classmethod
    def get_votes(cls, entity):
        result = cls.objects.filter(entity=entity).aggregate(Sum('vote'))
        return result['vote__sum'] or 0


class Tag(models.Model):
    keyword = models.CharField(max_length=30)

    def __str__(self):
        return self.keyword


class Question(Authorable, VotesMixin, models.Model):
    title = models.CharField(max_length=200)
    content = models.TextField()
    date = models.DateTimeField(default=timezone.now, verbose_name='publication date')
    tags = models.ManyToManyField(Tag, blank=True)
    correct_answer = models.OneToOneField(
            'Answer',
            on_delete=models.SET_NULL,
            blank=True, null=True,
            related_name='is_correct_to')

    objects = models.Manager()
    with_rating = WithRatingManager()

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        from django.urls import reverse
        return reverse('questions:question', args=[str(self.id)])

    def get_answers_count(self):
        return self.answers.count()

    def add_tags_from_list(self, tag_list):
        for t in tag_list:
            tag, _ = Tag.objects.get_or_create(keyword=t)
            self.tags.add(tag)

    @classmethod
    def create_with_tags(cls, author, title, content, date=timezone.now(), tag_list=[]):
        instance = cls(author=author,
                       title=title,
                       content=content,
                       date=date)
        instance.save()
        instance.add_tags_from_list(tag_list)
        return instance


class Answer(Authorable, VotesMixin, models.Model):
    content = models.TextField()
    date = models.DateTimeField(default=timezone.now)
    question = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='answers')

    objects = models.Manager()
    with_rating = WithRatingManager()

    def __str__(self):
        return f"{self.question} - {self.author}"

    @transaction.atomic
    def set_correct(self):
        question = self.question
        question.correct_answer = self
        question.save()

    def is_correct(self):
        return True if self.question.correct_answer == self else False


class AnswerVotes(Voteable, Authorable, models.Model):
    entity = models.ForeignKey(Answer, on_delete=models.CASCADE, related_name='votes')


class QuestionVotes(Voteable, Authorable, models.Model):
    entity = models.ForeignKey(Question, on_delete=models.CASCADE, related_name='votes')
