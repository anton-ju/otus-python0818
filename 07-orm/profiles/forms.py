from django import forms
from django.forms.widgets import ClearableFileInput

from .models import Profile, User


class ClearableImageFileInput(ClearableFileInput):
    """ Add image tag to template """
    template_name = 'profiles/widgets/clearable_image_file_input.html'


class CreateUserForm(forms.ModelForm):
    password = forms.CharField(widget=forms.PasswordInput)
    repeat_password = forms.CharField(widget=forms.PasswordInput)

    class Meta:
        model = User
        fields = [
            'username',
            'email',
            'password',
        ]


class UserForm(forms.ModelForm):

    class Meta:
        model = User
        fields = ('first_name', 'last_name', 'email')


class ProfileForm(forms.ModelForm):
    class Meta:
        model = Profile
        fields = ('avatar',)
        widgets = {'avatar': ClearableImageFileInput()}
