from django.contrib import messages
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth.views import LoginView, LogoutView
from django.contrib.auth.forms import UserCreationForm
from django.db import transaction
from django.shortcuts import render, redirect
from django.urls import reverse_lazy
from django.utils.translation import ugettext_lazy as _
from django.views.generic import CreateView, View

from .forms import UserForm, ProfileForm
from .models import User


class CreateUserView(CreateView):
    form_class = UserCreationForm
    success_url = reverse_lazy('profiles:profile')
    template_name = 'profiles/signup.html'

    def form_valid(self, form):
        c = {'form': form, }
        user = form.save(commit=False)
        password = form.cleaned_data['password1']
        repeat_password = form.cleaned_data['password2']
        if password != repeat_password:
            messages.error(self.request, "Passwords do not Match", extra_tags='alert alert-danger')
            return render(self.request, self.template_name, c)
        user.set_password(password)
        user.save()
        return super(CreateUserView, self).form_valid(form)


class LogoutUserView(LogoutView):
    next_page = reverse_lazy('questions:index')


class LoginUserView(LoginView):
    template_name = 'profiles/login.html'
    extra_context = {'next': reverse_lazy('questions:index')}


class UpdateProfileView(LoginRequiredMixin, View):
    def post(self, request, *args, **kwargs):
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, request.FILES, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            msg = 'Your profile was successfully updated!'
            messages.success(request, _(msg))
            return redirect('questions:index')
        else:
            messages.error(request, _('Please correct the error below.'))
        return render(request, 'profiles/profile.html', {
            'user_form': user_form,
            'profile_form': profile_form
        })

    def get(self, request, *args, **kwargs):
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
        return render(request, 'profiles/profile.html', {
            'user_form': user_form,
            'profile_form': profile_form
        })
