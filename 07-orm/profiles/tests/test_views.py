from django.contrib.auth import get_user_model
from django.test import TestCase


class LoginViewTest(TestCase):
    def test_login_view(self):
        response = self.client.get('/login/')
        self.assertEqual(response.status_code, 200)


class CreateUserView(TestCase):
    def test_create_user_view(self):
        response = self.client.get('/signup/')
        self.assertEqual(response.status_code, 200)


class UpdateUserView(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user('user1', password='user1pass')

    def test_update_user_view(self):
        self.client.force_login(self.user)
        response = self.client.get('/profile/')
        self.assertEqual(response.status_code, 200)


class LogoutUserView(TestCase):
    def setUp(self):
        self.user = get_user_model().objects.create_user('user1', password='user1pass')

    def test_logout_user_view(self):
        self.client.force_login(self.user)
        response = self.client.get('/logout/')
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, '/')
