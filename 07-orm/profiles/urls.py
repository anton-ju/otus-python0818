from django.urls import path

from . import views

app_name = 'profiles'
urlpatterns = [
    path('login/', views.LoginUserView.as_view(), name='login'),
    path('logout/', views.LogoutUserView.as_view(), name='logout'),
    path('signup/', views.CreateUserView.as_view(), name='signup'),
    path('profile/', views.UpdateProfileView.as_view(), name='profile'),
]
