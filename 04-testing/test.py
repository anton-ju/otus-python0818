import unittest
import scoring
import api
import hashlib
import datetime
import functools
import logging
from unittest.mock import MagicMock


logging.basicConfig()
logger = logging.getLogger(__name__)
logger.setLevel(logging.DEBUG)


def cases(cases):
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args):
            for c in cases:
                new_args = args + (c if isinstance(c, tuple) else (c,))
                f(*new_args)
        return wrapper
    return decorator


class TestStore(unittest.TestCase):
    def setUp(self):
        self.store = scoring.Store()
        self.store.cache_set('valid_key', 1.0)

    def test_valid_key_cache_get(self):
        result = self.store.cache_get('valid_key')
        self.assertEqual(result, 1.0)

    def test_invalid_key_cache_get(self):
        result = self.store.cache_get('invalid_key')
        self.assertIsNone(result)

    def test_invalid_connection_params(self):
        self.assertRaises(Exception, scoring.Store(port=1111))


class TestScoring(unittest.TestCase):
    def setUp(self):
        self.store = MagicMock()
        self.store.get = MagicMock(return_value='some_value')

    @cases([
        {"phone": "79175002040", "email": "stupnikov@otus.ru"},
        ])
    def test_get_score_cache_failed(self, params):
        self.store.cache_get.side_effect = RuntimeError('Cache unavailable')
        phone = params.get('phone')
        email = params.get('email')
        result = scoring.get_score(self.store, phone, email)
        self.assertEqual(result, 3.0)
        self.store.cache_get.reset_mock()

    @cases([
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000",
         "first_name": "a", "last_name": "b"},
        ])
    def test_get_score_cache_empty(self, params):
        self.store.cache_get.return_value = None
        result = scoring.get_score(self.store, params.get('phone'), params.get('email'))
        self.assertEqual(result, 3.0)

        result = scoring.get_score(self.store, None, None,
                                   gender=params.get('gender'),
                                   birthday=params.get('birthday'))
        self.assertEqual(result, 1.5)

        result = scoring.get_score(self.store, None, None,
                                   first_name=params.get('first_name'),
                                   last_name=params.get('last_name'))
        self.assertEqual(result, 0.5)
        self.store.cache_get.reset_mock()

    def test_get_interests_storage_error(self):
        self.store.get.side_effect = Exception('Redis error')
        with self.assertRaises(RuntimeError):
            scoring.get_interests(self.store, 'some_client_id')

    def test_get_interests_valid_cid(self):
        valid_cid = 12345
        expected_result = ['interest']
        self.store.get.return_value = expected_result
        self.assertEqual(expected_result, scoring.get_interests(self.store, valid_cid))
        self.store.get.assert_called_with("i:%s" % valid_cid)
        self.store.get.reset_mock()

    def test_get_interests_invalid_cid(self):
        cid = 12345
        expected_result = None
        self.store.get.return_value = expected_result
        self.assertEqual(expected_result, scoring.get_interests(self.store, cid))
        self.store.get.assert_called_with("i:%s" % cid)
        self.store.get.reset_mock()


class TestApi(unittest.TestCase):
    def setUp(self):
        self.context = {}
        self.headers = {}
        self.store = MagicMock()
        self.store.cache_get = MagicMock(return_value=2)
        self.store.get = MagicMock(return_value='some_value')

    def get_response(self, request):
        return api.method_handler({"body": request, "headers": self.headers}, self.context, self.store)

    def set_valid_auth(self, request):
        if request.get("login") == api.ADMIN_LOGIN:
            request["token"] = \
                hashlib.sha512((datetime.datetime.now().strftime("%Y%m%d%H") + api.ADMIN_SALT).encode()).hexdigest()
            self.context['is_admin'] = True
        else:
            msg = request.get("account", "") + request.get("login", "") + api.SALT
            request["token"] = hashlib.sha512(msg.encode()).hexdigest()

    def test_empty_request(self):
        _, code = self.get_response({})
        self.assertEqual(api.INVALID_REQUEST, code)

    @cases([
        {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "token": "", "arguments": {}},
        {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "token": "sdd", "arguments": {}},
        {"account": "horns&hoofs", "login": "admin", "method": "online_score", "token": "", "arguments": {}},
    ])
    def test_bad_auth(self, request):
        _, code = self.get_response(request)
        self.assertEqual(api.FORBIDDEN, code)

    @cases([
        {"account": "horns&hoofs", "login": "h&f", "method": "online_score"},
        {"account": "horns&hoofs", "login": "h&f", "arguments": {}},
        {"account": "horns&hoofs", "method": "online_score", "arguments": {}},
    ])
    def test_invalid_method_request(self, request):
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        self.assertEqual(api.INVALID_REQUEST, code)
#        self.assertTrue(len(response))

    @cases([
        {},
        {"phone": "7abcdefghij", "email": "stupnikov@otus.ru"},
        {"phone": "79175002040"},
        {"phone": "89175002040", "email": "stupnikov@otus.ru"},
        {"phone": "79175002040", "email": "stupnikovotus.ru"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": -1},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": "1"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.1890"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "XXX"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000", "first_name": 1},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000",
         "first_name": "s", "last_name": 2},
        {"phone": "79175002040", "birthday": "01.01.2000", "first_name": "s"},
        {"email": "stupnikov@otus.ru", "gender": 1, "last_name": 2},
    ])
    def test_invalid_score_request(self, arguments):
        request = {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        self.assertEqual(api.INVALID_REQUEST, code, arguments)
        #self.assertTrue(len(response))

    @cases([
        {"phone": "79175002040", "email": "stupnikov@otus.ru"},
        {"phone": 79175002040, "email": "stupnikov@otus.ru"},
        {"gender": 1, "birthday": "01.01.2000", "first_name": "a", "last_name": "b"},
        {"gender": 0, "birthday": "01.01.2000"},
        {"gender": 2, "birthday": "01.01.2000"},
        {"first_name": "a", "last_name": "b"},
        {"phone": "79175002040", "email": "stupnikov@otus.ru", "gender": 1, "birthday": "01.01.2000",
         "first_name": "a", "last_name": "b"},
    ])
    def test_ok_score_request(self, arguments):
        request = {"account": "horns&hoofs", "login": "h&f", "method": "online_score", "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        self.assertEqual(api.OK, code, arguments)
        score = response.get("score")
        self.assertTrue(isinstance(score, (int, float)) and score >= 0, arguments)
        self.assertEqual(sorted(self.context["has"]), sorted(arguments.keys()))

    def test_ok_score_admin_request(self):
        arguments = {"phone": "79175002040", "email": "stupnikov@otus.ru"}
        request = {"account": "horns&hoofs", "login": "admin", "method": "online_score", "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        self.assertEqual(api.OK, code)
        score = response.get("score")
        self.assertEqual(score, 42)

    @cases([
        {},
        {"date": "20.07.2017"},
        {"client_ids": [], "date": "20.07.2017"},
        {"client_ids": {1: 2}, "date": "20.07.2017"},
        {"client_ids": ["1", "2"], "date": "20.07.2017"},
        {"client_ids": [1, 2], "date": "XXX"},
    ])
    def test_invalid_interests_request(self, arguments):
        request = {"account": "horns&hoofs", "login": "h&f", "method": "clients_interests", "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        self.assertEqual(api.INVALID_REQUEST, code, arguments)
        #self.assertTrue(len(response))

    @cases([
        {"client_ids": [1, 2, 3], "date": datetime.datetime.today().strftime("%d.%m.%Y")},
        {"client_ids": [1, 2], "date": "19.07.2017"},
        {"client_ids": [0]},
    ])
    def test_ok_interests_request(self, arguments):
        request = {"account": "horns&hoofs", "login": "h&f", "method": "clients_interests", "arguments": arguments}
        self.set_valid_auth(request)
        response, code = self.get_response(request)
        self.assertEqual(api.OK, code, arguments)
        self.assertEqual(len(arguments["client_ids"]), len(response))
        self.assertTrue(all(v and isinstance(v, list) for i in v)
                        for v in response.values())
        self.assertEqual(self.context.get("nclients"),
                         len(arguments["client_ids"]))


if __name__ == "__main__":
    unittest.main()
