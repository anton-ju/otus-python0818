import hashlib
import json
import redis


class Store:
    def __init__(self, address='localhost', port=6378):
        self.client = redis.StrictRedis(host=address,
                                        port=port,
                                        db=0,
                                        socket_connect_timeout=3,
                                        socket_timeout=3,
                                        retry_on_timeout=True
                                        )

    def cache_get(self, key):
        result = self.client.get(key)
        return json.loads(result) if result else None

    def get(self, key):
        try:
            result = self.cache_get(key)
        except redis.exceptions.RedisError:
            raise Exception('Redis error')
        return result

    def cache_set(self, key, value, expire=3600):
        json_value = json.dumps(value)
        self.client.set(key, json_value)
        self.client.expire(key, expire)


def get_score(store, phone, email,
              birthday=None, gender=None, first_name=None, last_name=None):
    key_parts = [
        first_name or "",
        last_name or "",
        birthday or ""
    ]
    key = "uid:" + hashlib.md5("".join(key_parts).encode()).hexdigest()
    # try get from cache,
    # fallback to heavy calculation in case of cache miss
    try:
        score = store.cache_get(key) or 0
    except Exception:
        score = 0
    if score:
        return score
    if phone:
        score += 1.5
    if email:
        score += 1.5
    if birthday and gender:
        score += 1.5
    if first_name and last_name:
        score += 0.5
    # cache for 60 minutes
    store.cache_set(key, score,  60 * 60)
    return score


def get_interests(store, cid):
    try:
        r = store.get("i:%s" % cid)
    except Exception as e:
        raise RuntimeError("Unable to get interests: %s" % e)

    return r
