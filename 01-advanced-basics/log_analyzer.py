#!/usr/bin/env python
# -*- coding: utf-8 -*-


# log_format ui_short '$remote_addr  $remote_user $http_x_real_ip [$time_local] "$request" '
#                     '$status $body_bytes_sent "$http_referer" '
#                     '"$http_user_agent" "$http_x_forwarded_for" "$http_X_REQUEST_ID" "$http_X_RB_USER" '
#                     '$request_time';
import argparse
import json
import re
import os
import logging
import gzip
import collections
import statistics
import sys
from string import Template

config = {
    "REPORT_SIZE": 1000,
    "REPORT_DIR": "./reports",
    "LOG_DIR": "./log",
    "REPORT_TEMPLATE": "report.html",
    "MAX_ERRORS_PCT": 10,
}

FILE_NAME_REGEX = 'nginx-access-ui\.log-(?P<date>\d{8})\.gz'
FILE_NAME_SAMPLE = 'nginx-access-ui.log-20170630.gz'
LOG_LINE_REGEX = '(?P<remote_addr>\d+\.\d+\.\d+.\d+)\s' \
                 '(?P<remote_user>.+?)\s' \
                 '(?P<http_x_real_ip>.+?)\s' \
                 '\[(?P<time_local>.+?)\]\s"' \
                 '(?P<request>.+?)"\s' \
                 '(?P<status>\d+)\s' \
                 '(?P<body_bytes_sent>\d+)\s"' \
                 '(?P<http_referer>.+?)"\s' \
                 '"(?P<http_user_agent>.+?)"\s' \
                 '"(?P<http_x_forwarded_for>.+?)"\s' \
                 '"(?P<http_X_REQUEST_ID>.+?)"\s' \
                 '"(?P<http_X_RB_USER>.+?)"\s' \
                 '(?P<request_time>\d+(?:\.\d+)?)'

Log_file = collections.namedtuple('Log_file', ['file_name', 'date'])


def message(msg, error_level):
    """
    if error sys.exit() calls
    :param msg: message to write in log
    :param error_level:
    :return:
    """
    tell = {
        logging.INFO: logging.info,
        logging.ERROR: logging.error
    }
    tell[error_level](msg)
    if error_level >= logging.ERROR:
        raise RuntimeError(msg)


def get_log_file(log_dir):
    """
    :param log_dir:
    :return: returns namedtuple (filename, date) if not found returns (None, None)
    """
    try:
        listdir = os.listdir(log_dir)
        if listdir:
            for file in sorted(listdir, reverse=True):
                parsed = parse_file_name(file)
                if parsed:
                    l_file = Log_file(file_name=os.path.join(log_dir, file), date=parsed)
                    return l_file
                else:
                    return None, None
    except IOError:
        message('Invalid log directory', logging.ERROR)


def parse_file_name(file_name):
    """
    parses file name and extracts date from file name
    :param file_name:
    :return: if parsed return string parsed date from file_name, else None
    """
    if file_name:
        match = re.match(FILE_NAME_REGEX, file_name)
        if match:
            return match.groupdict().get('date')
    return None


def read_line(file_name):
    if file_name:
        with gzip.open(file_name, mode='rt', encoding='utf-8') as f:
            for line in f:
                yield line


def parse_line(line):
    """
    :param line: string from log file
    :return: dict
    """
    if line:
        match = re.match(LOG_LINE_REGEX, line)
        if match:
            return match.groupdict()


def url_from_request(request):
    """
    request sample 'GET /export/appinstall_raw/2017-06-29/ HTTP/1.0'
    :param request: string
    :return: string or None if Error
    """
    return request.split()[1]


def render_report(data, template_file):
    template_str = ''
    try:
        with open(template_file, encoding='utf-8') as f:
            template_str = f.read()
    except IOError:
        message(f"Error opening {template_file}", logging.ERROR)

    s = Template(template_str)
    return s.safe_substitute(table_json=data)


def save_report(data, file_name):
    try:
        with open(file_name, 'w', encoding='utf-8') as f:
            f.write(data)
        message(f'Report saved into {file_name}', logging.INFO)
    except IOError:
        message(f'Error writing report file {file_name}', logging.ERROR)


def load_config(config_current, config_file):
    try:
        with open(config_file, 'r') as f:
            config_json = f.read()
            if config_json == '':
                return
            new_config = json.loads(config_json, encoding='utf-8')
            if new_config:
                config_current.update(new_config)
    except IOError:
        message('Error opening config file', logging.ERROR)
    except json.JSONDecodeError:
        message('Invalid config file structure', logging.ERROR)


def find_report_file(file_name):
    """
    :param file_name:
    :return: True if found file_name else False
    """
    return os.path.isfile(file_name)


def parse_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('--config', action='store', help='Config file', default='log_analyzer.cfg')
    return parser.parse_args()


def read_log_file(lines, file_name):
    """

    :param lines:
    :param file_name:
    :return:
        Result.urls: dictionary with time list
        Result.errors_pct: percent of errors while reading file
    """
    url_dict = collections.defaultdict(list)
    errors_cnt = 0
    lines_cnt = 0

    for line in lines(file_name):
        url = ''
        lines_cnt += 1
        parsed_line = parse_line(line)
        if parsed_line is None:
            message(f'Invalid log record format in line# {lines_cnt}', logging.INFO)
            errors_cnt += 1
        try:
            url = url_from_request(parsed_line.get('request'))
            if url:
                time = float(parsed_line.get('request_time'))
                url_dict[url].append(time)
            else:
                errors_cnt += 1
        except IndexError:
            message(f'Invalid request in log file line# {lines_cnt}', logging.INFO)
            errors_cnt += 1

    errors_pct = 100 * errors_cnt / lines_cnt
    return url_dict, errors_pct


def calc_stats(url_dict):
    count_dict = {url: len(time_list) for url, time_list in url_dict.items()}
    time_sum_dict = {url: sum(time_list) for url, time_list in url_dict.items()}
    total_time_sum = sum(time_sum_dict.values())
    total_count = sum(count_dict.values())
    report_data = []
    for url, time in sorted(time_sum_dict.items(), key=lambda x: x[1], reverse=True)[:config['REPORT_SIZE']]:
        data_item = {
            'url': url,
            'count': count_dict[url],
            'count_perc': round(100 * count_dict[url] / total_count, 3),
            'time_sum': round(time, 3),
            'time_perc': round(100 * time_sum_dict[url] / total_time_sum, 3),
            'time_avg': round(time_sum_dict[url] / count_dict[url], 3),
            'time_max': round(max(url_dict[url]), 3),
            'time_min': round(min(url_dict[url]), 3),
            'time_med': round(statistics.median(url_dict[url]), 3)
        }
        report_data.append(data_item)

    return report_data


def main():
    # configure logging and loading config from file
    logging.basicConfig(
                        format='[%(asctime)s] %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S',
                        level=logging.INFO
                        )
    args = parse_arguments()

    try:
        load_config(config, args.config)

        logging.basicConfig(filename=config.get('LOG_FILE', None))

        log_file_name, log_file_date = get_log_file(config.get('LOG_DIR'))
        if log_file_name is None:
            message(f'Log file not found in {config.get("LOG_DIR")}', logging.ERROR)

        report_file_path = os.path.join(config['REPORT_DIR'], ''.join(('report-', log_file_date, '.html')))
        if find_report_file(report_file_path):
            message(f'Report {report_file_path} allready exists.', logging.ERROR)

        url_time_dict, errors_pct = read_log_file(read_line, log_file_name)
        if errors_pct >= config["MAX_ERRORS_PCT"]:
            message('Max error level percent exceeded!', logging.ERROR)

        report_data = calc_stats(url_time_dict)
        report_template_file = os.path.join(config['REPORT_DIR'], config['REPORT_TEMPLATE'])
        report = render_report(json.dumps(report_data), report_template_file)
        save_report(report, report_file_path)
    except RuntimeError:
        sys.exit(-1)
    except Exception as e:
        # all unexpected exceptions which is not caught in procedures
        logging.exception(str(e))
        sys.exit(-1)


if __name__ == "__main__":
    main()