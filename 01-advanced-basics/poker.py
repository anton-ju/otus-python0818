#!/usr/bin/env python
# -*- coding: utf-8 -*-

# -----------------
# Реализуйте функцию best_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. У каждой карты есть масть(suit) и
# ранг(rank)
# Масти: трефы(clubs, C), пики(spades, S), червы(hearts, H), бубны(diamonds, D)
# Ранги: 2, 3, 4, 5, 6, 7, 8, 9, 10 (ten, T), валет (jack, J), дама (queen, Q), король (king, K), туз (ace, A)
# Например: AS - туз пик (ace of spades), TH - дестяка черв (ten of hearts), 3C - тройка треф (three of clubs)

# Задание со *
# Реализуйте функцию best_wild_hand, которая принимает на вход
# покерную "руку" (hand) из 7ми карт и возвращает лучшую
# (относительно значения, возвращаемого hand_rank)
# "руку" из 5ти карт. Кроме прочего в данном варианте "рука"
# может включать джокера. Джокеры могут заменить карту любой
# масти и ранга того же цвета, в колоде два джокерва.
# Черный джокер '?B' может быть использован в качестве треф
# или пик любого ранга, красный джокер '?R' - в качестве черв и бубен
# любого ранга.

# Одна функция уже реализована, сигнатуры и описания других даны.
# Вам наверняка пригодится itertoolsю
# Можно свободно определять свои функции и т.п.
# -----------------

from collections import Counter
import itertools
from decimal import *
import math

SUITS = ['C', 'S', 'H', 'D']
BJ_SUITS = [SUITS[0], SUITS[1]]
RJ_SUITS = [SUITS[2], SUITS[3]]
RAKNS = {
    '2': 2,
    '3': 3,
    '4': 4,
    '5': 5,
    '6': 6,
    '7': 7,
    '8': 8,
    '9': 9,
    'T': 10,
    'J': 11,
    'Q': 12,
    'K': 13,
    'A': 14
}

def hand_rank(hand):
    """Возвращает значение определяющее ранг 'руки'"""
    ranks = card_ranks(hand)
    if straight(ranks) and flush(hand):
        return (8, max(ranks))
    elif kind(4, ranks):
        return (7, kind(4, ranks), kind(1, ranks))
    elif kind(3, ranks) and kind(2, ranks):
        return (6, kind(3, ranks), kind(2, ranks))
    elif flush(hand):
        return (5, max(ranks))
    elif straight(ranks):
        return (4, max(ranks))
    elif kind(3, ranks):
        set_rank = kind(3, ranks)
        ranks.remove(set_rank)
        ranks.remove(set_rank)
        ranks.remove(set_rank)
        return (3, set_rank, max(ranks))
    elif two_pair(ranks):
        tp = two_pair(ranks)
        ranks.remove(tp[0])
        ranks.remove(tp[0])
        ranks.remove(tp[1])
        ranks.remove(tp[1])
        return (2, tp[0], tp[1], max(ranks))
    elif kind(2, ranks):
        pair = kind(2, ranks)
        ranks.remove(pair)
        ranks.remove(pair)
        return (1, pair, max(ranks))
    else:
        return (0, max(ranks))


def hand_value(hand):
    """
    возвращает число определяющее силу руки, чем больше число тем сильнее рука
    первые 0-15 битов кикер или младшая пара
    16-31 младшая пара
    32--47 старшая пара
    48-56 ранг руки
    """
    rank = hand_rank(hand)
    ret_value = 0
    KICKER_OFFSET = 0
    LOW_PAIR_OFFSET = 16
    HIGH_PAIR_OFFSET = 32
    RANK_OFFSET = 48

    if len(rank) == 2:
        ret_value = Decimal(math.pow(2, RANK_OFFSET + rank[0]) + math.pow(2, KICKER_OFFSET + rank[1]))
    elif len(rank) == 3:
        ret_value = Decimal(math.pow(2, RANK_OFFSET + rank[0]) + math.pow(2, HIGH_PAIR_OFFSET + rank[1])
        + math.pow(2, KICKER_OFFSET + rank[2]))
    elif len(rank) == 4:
        ret_value = Decimal(math.pow(2, RANK_OFFSET + rank[0]) + math.pow(2, HIGH_PAIR_OFFSET + rank[1])
        + math.pow(2, LOW_PAIR_OFFSET + rank[2]) + math.pow(2, KICKER_OFFSET + rank[3]))

    return ret_value


def card_ranks(hand):
    """Возвращает список рангов (его числовой эквивалент),
    отсортированный от большего к меньшему"""
    # return sorted([RAKNS[x[0]] for x in hand], reverse=True)
    return sorted(["23456789TJQKA".index(rank[0])+2 for rank in hand], reverse=True)


def flush(hand):
    """Возвращает True, если все карты одной масти"""
    n = 5
    # вроде бы должно быть 5 карт одной масти
    suits = [x[1] for x in hand]
    suits_comb = itertools.combinations(suits, 5)
    for comb in suits_comb:
        if len(set(comb)) == 1:
            return True
    # for value in Counter(suits).values():
    #     if value >= n:
    #         return True
    return False


def straight(ranks):
    """Возвращает True, если отсортированные ранги формируют последовательность 5ти,
    где у 5ти карт ранги идут по порядку (стрит)"""
    all_ranks = sorted(list(RAKNS.values()), reverse=True)
    #все возможные комбинации стритов
    all_straights = [all_ranks[x:x+5] for x in range(len(all_ranks)-4)]
    all_hand_combs = [ranks[x:x+5] for x in range(len(ranks)-4)]
    for comb in all_hand_combs:
        if comb in all_straights:
            return True
        else:
            return False


def kind(n, ranks):
    """Возвращает первый ранг, который n раз встречается в данной руке.
    Возвращает None, если ничего не найдено"""
    cnt = Counter(ranks)
    for key in sorted(cnt.keys(), reverse=True):
        if cnt.get(key) == n:
            return key
    return None


def two_pair(ranks):
    """Если есть две пары, то возврщает два соответствующих ранга,
    иначе возвращает None"""
    ranks_ = list(ranks)
    pair1 = kind(2, ranks_)
    if pair1:
        ranks_.pop(ranks_.index(pair1))
        ranks_.pop(ranks_.index(pair1))
        pair2 = kind(2, ranks_)
        if pair2:
            return [pair1, pair2]

    return None


# def find_best_hand(all_hands):
#     """
#     перебираем все руки, выбираем руку с максимальным hand_value()
#     :param all_hands: list of hands
#     :return hand:
#     """
#     max_value = 0
#     res_hand = None
#     for h in all_hands:
#         value = hand_value(h)
#         # print(value)
#         if value > max_value:
#             max_value = value
#             res_hand = h
#     return list(res_hand)


def best_hand(hand):
    """Из "руки" в 7 карт возвращает лучшую "руку" в 5 карт """
    all_5card_hands = itertools.combinations(hand, 5)
    return max(all_5card_hands, key=hand_value)
    # return find_best_hand(all_5card_hands)



def discard_joker(hand, simbol):
    if simbol in hand:
        hand.remove(simbol)
        if simbol == '?B':
            joker_cards = [''.join(x) for x in itertools.product(RAKNS.keys(), BJ_SUITS)]
        elif simbol =='?R':
            joker_cards = [''.join(x) for x in itertools.product(RAKNS.keys(), RJ_SUITS)]

        all_hands = [list(itertools.chain(hand, [x])) for x in joker_cards if x not in hand]
        # all_7card_hands = itertools.chain(all_7card_hands,
        #                                   [list(itertools.chain(hand, [x])) for x in all_blackjoker_cards if
        #                                    x not in hand])
        return all_hands
    return []

def best_wild_hand(hand):
    """best_hand но с джокерами"""
    all_5card_hands = []
    all_hands = []

    if ('?R' in hand) and ('?B' in hand):
        hands = discard_joker(hand, "?R")
        for h in hands:
            all_hands = itertools.chain(all_hands, discard_joker(h, '?B'))
    elif '?R' in hand:
        all_hands = discard_joker(hand, "?R")
    elif '?B' in hand:
        all_hands = discard_joker(hand, "?B")
    else:
        return best_hand(hand)

    for h in all_hands:
        for i in itertools.combinations(h, 5):
            all_5card_hands.append(i)

    return max(all_5card_hands, key=hand_value)


def test_best_hand():
    print("test_best_hand...")

    assert (sorted(best_hand("6C 7C 8C 9C TC 5C JS".split()))
            == ['6C', '7C', '8C', '9C', 'TC'])
    assert (sorted(best_hand("TD TC TH 7C 7D 8C 8S".split()))
            == ['8C', '8S', 'TC', 'TD', 'TH'])
    assert (sorted(best_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    print('OK')


def test_best_wild_hand():
    print("test_best_wild_hand...")
    assert (sorted(best_wild_hand("6C 7C 8C 9C TC 5C ?B".split()))
            == ['7C', '8C', '9C', 'JC', 'TC'])
    assert (sorted(best_wild_hand("TD TC 5H 5C 7C ?R ?B".split()))
            == ['7C', 'TC', 'TD', 'TH', 'TS'])
    assert (sorted(best_wild_hand("JD TC TH 7C 7D 7S 7H".split()))
            == ['7C', '7D', '7H', '7S', 'JD'])
    print('OK')


def test_cards_ranks():
    print("test_card_ranks")
    hand = "JD TC TH 7C 7D 7S 7H".split()
    try:
        assert (card_ranks(hand)) == [11, 10, 10, 7, 7, 7, 7]
        print('OK')
    except AssertionError:
        print('assertion error')
        print(card_ranks(hand))


def test_kind():
    print("test_kind")
    ranks = [11, 10, 10, 7, 7, 7, 7]
    try:
        assert (kind(2, ranks)) == 10
        assert (kind(4, ranks)) == 7
        assert (kind(3, ranks)) == None
        print('OK')
    except AssertionError:
        print('assertion error')
        print(kind(2, ranks))
        print(kind(4, ranks))
        print(kind(3, ranks))


def test_flush():
    print('test_flush')
    hand1 = "JC TC AC 7C 7D 7S QC".split()
    hand2 = "JD TC AC 7C 7D 7S QC".split()
    hand3 = "6C 7C 8C 9C TC 5C JS".split()
    try:
        assert (flush(hand1))
        assert not(flush(hand2))
        assert (flush(hand3))
        print('OK')
    except AssertionError:
        print('assertion error')
        print(flush(hand1))
        print(flush(hand2))
        print(flush(hand3))


def test_two_pairs():
    print('test_two_pairs')
    hand1 = [10, 10, 8, 7, 4, 2, 2]
    hand2 = [10, 9, 8, 4, 5, 2, 2]
    try:
        assert (two_pair(hand1) == [10, 2])
        assert (two_pair(hand2)) is None
        print('OK')
    except AssertionError:
        print('assertion error')
        print(two_pair(hand1))
        print(two_pair(hand2))

def test_straight():
    print('test_straight')
    hand1 = [9,8,7,6,5]
    hand2 = [13,12,10,8,9]
    try:
        assert (straight(hand1))
        assert not(straight(hand2))
        print('OK')
    except AssertionError:
        print('assertion error')
        print(straight(hand1))
        print(straight(hand2))

def test_hand_rank():
    print('hand_rank')
    straight_flush_hand = ['5C','6C','7C','8C','9C']
    quads_hand = ['KC','KD','KS','KH','9C']
    full_hand = ['KC', 'KD', 'QS', 'QH', 'QC']
    flush_hand = ['2C','6C','7C','8C','9C']
    straight_hand = ['5D','6S','7C','8C','9C']
    set_hand = ['AC', 'KD', 'QS', 'QH', 'QC']
    two_pair_hand = ['KC', 'KD', 'QS', 'AH', 'QC']
    pair_hand = ['2C', '2D', 'AS', 'JH', 'QC']
    high_card_hand = ['KC', '2D', 'TS', '3H', 'QC']

    try:
        assert (hand_rank(straight_flush_hand) == (8, 9))
        assert (hand_rank(quads_hand) == (7, 13, 9))
        assert (hand_rank(full_hand) == (6, 12, 13))
        assert (hand_rank(flush_hand) == (5, 9))
        assert (hand_rank(straight_hand) == (4, 9))
        assert (hand_rank(set_hand) == (3, 12, 14))
        assert (hand_rank(two_pair_hand) == (2, 13, 12, 14))
        assert (hand_rank(pair_hand) == (1, 2, 14))
        assert (hand_rank(high_card_hand) == (0, 13))
        print('OK')
    except AssertionError:
        print('assertion error')
        print(hand_rank(straight_flush_hand))
        print(hand_rank(quads_hand))
        print(hand_rank(full_hand))
        print(hand_rank(flush_hand))
        print(hand_rank(straight_hand))
        print(hand_rank(set_hand))
        print(hand_rank(two_pair_hand))
        print(hand_rank(pair_hand))
        print(hand_rank(high_card_hand))


if __name__ == '__main__':
    # test_best_hand()
    # test_best_wild_hand()


    test_cards_ranks()
    test_kind()
    test_flush()
    test_two_pairs()
    test_straight()
    test_hand_rank()

    test_best_hand()
    test_best_wild_hand()


