#!/usr/bin/env python
# -*- coding: utf-8 -*-

from functools import update_wrapper, partial, wraps
from weakref import WeakKeyDictionary


def disable(func):
    '''
    Disable a decorator by re-assigning the decorator's name
    to this function. For example, to turn off memoization:

    >>> memo = disable

    '''
    try:
        wrapped_func = getattr(func, '__wrapped__')
        return wrapped_func
    except AttributeError:
        return func


def decorator(wrapped):
    '''
    Decorate a decorator so that it inherits the docstrings
    and stuff from the function it's decorating.
    '''

    return partial(update_wrapper, wrapped=wrapped,
                   assigned=['__doc__'], updated=['__dict__'])


def countcalls(func):
    '''Decorator that counts calls made to the function decorated.'''
    @wraps(func)
    def wrapper(*args, **kwargs):
        result = func(*args, **kwargs)
        wrapper.calls += 1
        return result

    wrapper.calls = 0
    return wrapper


def memo(func):
    '''
    Memoize a function so that it caches all return values for
    faster future lookups.
    '''

    @wraps(func)
    def wrapper(*args):
        try:
            cached = getattr(wrapper, 'cached')
            return cached[(func, *args)]
        except (AttributeError, KeyError):
            result = func(*args)
            wrapper.cached[(func, *args)] = result
            return result

    # wrapper.cached = WeakKeyDictionary()
    wrapper.cached = dict()
    return wrapper


def n_ary(func):
    '''
    Given binary function f(x, y), return an n_ary function such
    that f(x, y, z) = f(x, f(y,z)), etc. Also allow f(x) = x.
    '''

    @wraps(func)
    def wrapper(*args):
        if len(args) > 2:
            result = func(args[0], wrapper(*args[1:]))
        elif len(args) == 2:
            result = func(args[0], args[1])
        else:
            result = args[0]
        return result
    return wrapper


def trace(func, symb='____'):
    '''Trace calls made to function decorated.

    @trace("____")
    def fib(n):
        ....

    >>> fib(3)
     --> fib(3)
    ____ --> fib(2)
    ________ --> fib(1)
    ________ <-- fib(1) == 1
    ________ --> fib(0)
    ________ <-- fib(0) == 1
    ____ <-- fib(2) == 2
    ____ --> fib(1)
    ____ <-- fib(1) == 1
     <-- fib(3) == 3

    '''
    def decorator(func):
        @wraps(func)
        def wrapper(*args, **kwargs):
            print(f'{wrapper.calls * symb}-->{func.__name__}({args}, {kwargs})')
            wrapper.calls += 1
            result = func(*args, **kwargs)
            wrapper.calls -= 1
            print(f'{wrapper.calls * symb}<--{func.__name__}({args}, {kwargs}) == {result}')

            return result

        wrapper.calls = 1
        return wrapper
    return decorator


@countcalls
@n_ary
@memo
def foo(a, b):
    return a + b


@countcalls
@memo
@n_ary
def bar(a, b):
    return a * b


@countcalls
@memo
@trace("####")
def fib(n):
    """Some doc"""
    return 1 if n <= 1 else fib(n-1) + fib(n-2)


def main():
    print(foo(4, 3, 5, 6))
    print(foo(4, 3, 2))
    print(foo(4, 3))
    print(foo.__dict__)
    print("foo was called", foo.calls, "times")

    print(bar(4, 3))
    print(bar(4, 3, 2))
    print(bar(4, 3, 2, 1))
    print("bar was called", bar.calls, "times")

    print(fib.__doc__)
    fib(3)
    print(fib.calls, 'calls made')


if __name__ == '__main__':
    main()
