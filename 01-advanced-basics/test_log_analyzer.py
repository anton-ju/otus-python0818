import functools
from unittest import TestCase
import log_analyzer


def cases(cases):
    def decorator(f):
        @functools.wraps(f)
        def wrapper(*args):
            for c in cases:
                new_args = args + (c if isinstance(c, tuple) else (c,))
                f(*new_args)
        return wrapper
    return decorator


class TestSuite(TestCase):
    def setUp(self):
        pass

    @cases([
        {},
    ])
    def test_get_log_file(self, log_dir):
        # log_file_name, log_file_date = log_analyzer.get_log_file()
        # self.fail()
        pass

    @cases([
        {'test':'', 'result':None},
        {'test': 'nginx-access-ui.log-20170730.gz', 'result': '20170730'},
        {'test': 'nginx-access-ui.log-20170730.smth', 'result': None},
        {'test': 'another.log-20170730.gz', 'result': None},
        {'test': 'nginx-access-ui.log-2017073.gz', 'result': None},
    ])
    def test_parse_file_name(self, arguments):
        self.assertEqual(arguments['result'], log_analyzer.parse_file_name(arguments['test']))