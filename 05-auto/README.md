## HTTP Server from scratch

### Synchronous Multi-Threading HTTP Server
* Scalable, uses workers pool to handle requests
* Handles GET and PUT requests
* Serves files from specified directory
---
### Usage:

`$ python httpd.py [option] [value]`
<dl>
    <dt>-p, --port:</dt>
    <dd>server port, default 8080</dd>
    <dt>-w, --nworkers:</dt>
    <dd>number of workers in pool, default 8</dd>
    <dt>-r, --root:</dt>
    <dd>root directory path, default http-test-suite</dd>
    <dt>-l, --log:</dt>
    <dd>enables logging to file</dd>
</dl>

### Results of Apache HTTP server benchmarking tool

```
$ ab -n 50000 -c 100 -r http://localhost:8080/

This is ApacheBench, Version 2.3 <$Revision: 1807734 $>
Copyright 1996 Adam Twiss, Zeus Technology Ltd, http://www.zeustech.net/
Licensed to The Apache Software Foundation, http://www.apache.org/

Benchmarking localhost (be patient)


Server Software:        OTUServer
Server Hostname:        localhost
Server Port:            8080

Document Path:          /
Document Length:        0 bytes

Concurrency Level:      100
Time taken for tests:   89.220 seconds
Complete requests:      50000
Failed requests:        0
Non-2xx responses:      50000
Total transferred:      5900000 bytes
HTML transferred:       0 bytes
Requests per second:    560.41 [#/sec] (mean)
Time per request:       178.440 [ms] (mean)
Time per request:       1.784 [ms] (mean, across all concurrent requests)
Transfer rate:          64.58 [Kbytes/sec] received

Connection Times (ms)
              min  mean[+/-sd] median   max
Connect:        0    2  42.4      0    1029
Processing:     1  176 221.4    176   13485
Waiting:        0  176 221.4    176   13485
Total:          3  178 248.4    176   14490

Percentage of the requests served within a certain time (ms)
  50%    176
  66%    177
  75%    179
  80%    181
  90%    185
  95%    191
  98%    197
  99%    201
 100%  14490 (longest request)
```