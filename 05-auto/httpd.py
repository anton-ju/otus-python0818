from optparse import OptionParser
from datetime import datetime
from threading import Thread, Condition, Event

import logging
import selectors
import socket
import os
import urllib
import mimetypes
import queue
import sys
import time

OK = 200
BAD_REQUEST = 400
FORBIDDEN = 403
NOT_FOUND = 404
INVALID_REQUEST = 422
INTERNAL_ERROR = 500
METHOD_NOT_ALLOWED = 405

ERRORS = {
    OK: "OK",
    BAD_REQUEST: "Bad Request",
    FORBIDDEN: "Forbidden",
    NOT_FOUND: "Not Found",
    INVALID_REQUEST: "Invalid Request",
    INTERNAL_ERROR: "Internal Server Error",
    METHOD_NOT_ALLOWED: "Method Not Allowed"
}

SERVER = 'OTUServer 0.1'
"""
Synchronous Multi-Threading server
"""

def log_message(msg):
    logger.info(msg)


def log_error(msg):
    logger.error(msg)


class Worker(Thread):
    def __init__(self, *args, **kwargs):
        if kwargs:
            self._new_job_cond = kwargs.pop('condition')
            self._job_queue = kwargs.pop('queue')
            self._shutdown_event = kwargs.pop('shutdown')
            self._finished_queue = kwargs.pop('finished')

        super().__init__(*args, **kwargs)

    def run(self):
        with self._new_job_cond:
            while self._job_queue.empty():
                self._new_job_cond.wait()
            try:
                if self._shutdown_event():
                    return
                self._target, self._args = self._job_queue.get()
                # if None in queue do nothing quit
                if self._target:
                    super().run()
                    self._finished_queue.put(self)
            except queue.Empty:
                pass


class ThreadPool:
    def __init__(self, max_threads=8):
        self._max_threads = max_threads
        self._threads = []
        self.queue = queue.Queue()
        self.new_job = Condition()
        self.finished = Condition()
        self.finished_queue = queue.Queue()
        self._shutdown = Event()
        self._shutdown.clear()
        self.is_shutdown = False
        self._submited = 0
        self._start_threads(self._max_threads)
        maintainer = Thread(target=self._maintain,
                            name='maintainer',
                            args=(self,))
        maintainer.setDaemon(True)
        maintainer.start()

    def _maintain(self, *args, **kwargs):
        """Remove terminated threads from self._thread and adjust number
        of started threads to self._max_threads
        """
        while not self.is_shutdown:
            for _ in range(self.finished_queue.qsize()):
                t = self.finished_queue.get(block=False)
                t.join()
                self._threads.remove(t)
                number_to_start = self._max_threads - len(self._threads)
                self._start_threads(number_to_start)

            time.sleep(0.1)

    def _start_threads(self, nthreads):
        for _ in range(nthreads):
            t = Worker(condition=self.new_job,
                       queue=self.queue,
                       shutdown=self._shutdown.is_set,
                       finished=self.finished_queue)
            t.start()
            self._threads.append(t)

    def submit(self, target, args):
        """Add new job to queue, notify workers
        """
        job = (target, args)
        with self.new_job:
            self.queue.put(job)
            self.new_job.notify()
            self._submited += 1

    def join_threads(self):
        for t in self._threads:
            t.join(timeout=2)

    def shutdown(self):
        """shutdown pool and all worker threads
        """
        self._shutdown.set()
        self.is_shutdown = True
        with self.new_job:
            self.queue.put((None, None))
            self.new_job.notify_all()
        self.join_threads()


class HTTPHandler:
    def __init__(self, request, client_address, root_dir):
        self.request = request
        self.client_address = client_address
        self.root_dir = root_dir
        self.recv_data = b''
        self.response = []
        self.protocol_version = "HTTP/1.1"
        self.headers = {
            'Server': SERVER,
            'Date': '',
            'Content-type': '',
            'Content-Length': 0
        }

    def handle(self):
        try:
            while True:
                r = self.request.recv(8096)
                if r:
                    self.recv_data += r
                    if b'\r\n\r\n' in self.recv_data:
                        break
                else:
                    raise OSError('Unexpected connection close')

            if self.parse_request():
                mname = 'do_' + self.command
                if not hasattr(self, mname):
                    self.error(METHOD_NOT_ALLOWED)
                    return
                method = getattr(self, mname)
                method()
                self.send_response()
            else:
                self.error(INVALID_REQUEST)

        except Exception as e:
            log_error(e)
        finally:
            try:
                self.request.close()
            except OSError:
                pass

    def parse_request(self):
        """parse request
        returns: True if success
        """
        requestline = str(self.recv_data, 'iso-8859-1')
        requestline = requestline.rstrip('\r\n')
        self.requestline = requestline
        words = requestline.split()
        if len(words) >= 3:
            command, path, version = words[:3]
        else:
            return False
        self.command, self.path, self.request_version = command, path, version

        return True

    def error(self, code):
        """send error """
        self.add_code(code)
        self.end_headers()
        self.send_response()

    def add_code(self, code):
        """append code to response list"""
        response = '%s %d %s\r\n' % (self.protocol_version, code, ERRORS[code])
        response = response.encode('latin-1', 'strict')
        self.response.append(response)

    def add_header(self, keyword, value):
        """append header to response list"""
        self.response.append(
            ("%s: %s\r\n" % (keyword, value)).encode('latin-1', 'strict'))

    def send_response(self):
        self.request.sendall(b"".join(self.response))
        self.response = []

    def do_GET(self):
        fpath = self.common_method_handler()
        if fpath:
            with open(fpath, 'rb') as f:
                self.request.setblocking(True)
                self.request.sendfile(f)

    def do_HEAD(self):
        self.common_method_handler()

    def common_method_handler(self):
        """Common method for all handlers
        returns: real file path
        """
        fpath = self.get_real_path(self.path)

        if not fpath or not os.path.exists(fpath):
            self.error(NOT_FOUND)
            return

        try:
            ctype = self.get_ctype(fpath)
            fs = os.stat(fpath).st_size
            self.headers.update({'Content-type': ctype})
            self.headers.update({'Content-Length': fs})
        except Exception as e:
            log_error(e)
            return

        self.add_code(OK)
        self.end_headers()
        self.send_response()
        return fpath

    def end_headers(self):
        """Put all headers into self.response and append CLRF"""
        self.headers.update({'Date': datetime.now()})
        for key, value in self.headers.items():
            self.add_header(key, value)
        self.response.append(b"\r\n")

    def get_ctype(self, path):
        if not path:
            return
        name = os.path.basename(path)
        ctype = mimetypes.guess_type(name)[0]
        return ctype

    def get_real_path(self, path):
        """translate uri into system path and return if exists
        """
        path = path.split('?', 1)[0]
        path = path.split('#', 1)[0]
        path = path.lstrip('/')
        trailing_slash = path.rstrip().endswith('/')
        path = urllib.parse.unquote(path)
        path = os.path.normpath(path)
        path = os.path.join(self.root_dir, path)
        if os.path.isdir(path) or trailing_slash:
            result = os.path.join(path, 'index.html')
        else:
            result = path

        return result


class HTTPServer:

    def __init__(self, server_address,
                 root_dir,
                 RequestHandlerClass,
                 nworkers=10,
                 request_queue_size=10,
                 bind_and_activate=True):

        self.server_address = server_address
        self.root_dir = root_dir
        self.RequestHandlerClass = RequestHandlerClass
        self.nworkers = nworkers
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.threads = []
        self._rbuffer = None
        self.workers_pool = ThreadPool(nworkers)
        self.request_queue_size = request_queue_size

        if bind_and_activate:
            self.server_activate()

    def server_activate(self):

        try:
            self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
            self.socket.bind(self.server_address)
            self.socket.setblocking(False)
            self.server_address = self.socket.getsockname()
            self.socket.listen(self.request_queue_size)
        except Exception as e:
            log_error('Exception while starting server %s ' % e)
            self.socket.close()
            self.workers_pool.shutdown()
            raise e

    def serve_forever(self):
        """main loop
        """
        selector.register(self.socket, selectors.EVENT_READ)

        try:
            while True:
                events = selector.select(timeout=None)
                if events:
                    self.handle_request()
        except KeyboardInterrupt:
            logger.debug("keyboard interrupt, exiting")
        finally:
            try:
                selector.close()
                self.socket.close()
            except OSError:
                pass

            self.workers_pool.shutdown()

    def handle_request(self):
        """Get request and put it into workers pull
        """
        request, addr = self.get_request()
        if request:
            self.workers_pool.submit(self.handle_request_thread,
                                     (request, addr))

    def get_request(self):
        """Get the request and client address from the socket.
        """
        request, addr = None, None
        try:
            request, addr = self.socket.accept()
            log_message('Accepted connection from %s' % addr[0])
        except Exception as e:
            log_error('while accepting request from %s: %s' % (addr[0], e))

        return request, addr

    def handle_request_thread(self, request, addr):
        """Handle one request
        """
        try:
            self.RequestHandlerClass(request, addr, self.root_dir).handle()
        except Exception as e:
            log_error('while handling request from %s: %s' % (addr[0], e))
        finally:
            request.close()

    def fileno(self):
        return self.socket.fileno()


def configure_logger(filename=None):
    logging.basicConfig(filename=filename, level=logging.DEBUG,
                        format='[%(asctime)s] %(levelname).1s %(message)s',
                        datefmt='%Y.%m.%d %H:%M:%S')
    logger = logging.getLogger(__name__)

    if filename:
        fh = logging.FileHandler(filename)
        logger.addHandler(fh)
    return logger


if __name__ == "__main__":
    op = OptionParser()
    op.add_option("-p", "--port", action="store", type=int, default=8080)
    op.add_option("-w", "--nworkers", action="store", default=8)
    op.add_option("-r", "--root", action="store", default='http-test-suite')
    op.add_option("-l", "--log", action="store", default=None)
    (opts, args) = op.parse_args()
    logger = configure_logger(opts.log)
    selector = selectors.DefaultSelector()
    root_dir = opts.root
    if not os.path.isabs(root_dir):
        root_dir = os.path.join(os.getcwd(), root_dir)
    server = HTTPServer(("localhost", opts.port),
                        root_dir,
                        HTTPHandler,
                        int(opts.nworkers))
    log_message("Starting in %s at %s port" % (root_dir, opts.port))

    try:
        server.serve_forever()
    except KeyboardInterrupt:
        pass
else:
    logger = configure_logger(None)
